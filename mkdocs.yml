site_description: Un travail sur les Ressources Éducatives Libres, aka REL

# Contenu généré depuis les paramètres de votre espace gitlab
site_name: !!python/object/apply:os.getenv ["CI_PROJECT_TITLE"]
site_url: !!python/object/apply:os.getenv ["CI_PAGES_URL"]
site_author: !!python/object/apply:os.getenv ["CI_PROJECT_ROOT_NAMESPACE"]

copyright: |
    Copyright © 2023 <DRANE - Site de Lyon>
    <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a>.

docs_dir: docs

nav:
  - "Accueil": index.md
  - ... | regex=^(?:(?!canevas)).*$
  - "Conclusion" : Conclusion.md


theme:
    favicon: assets/favicon.ico
    logo: assets/logo-rel.svg
    name: material
    font: false                     # RGPD ; pas de fonte Google
    language: fr                    # français
    palette:                        # Palettes de couleurs jour/nuit
      - media: "(prefers-color-scheme: light)"
        scheme: default
        primary: blue
        accent: blue
        toggle:
            icon: material/weather-sunny
            name: Passer au mode nuit
      - media: "(prefers-color-scheme: dark)"
        scheme: slate
        primary: indigo
        accent: indigo
        toggle:
            icon: material/weather-night
            name: Passer au mode jour
    features:
        - navigation.instant
        - navigation.tabs
        - navigation.top
        - toc.integrate
        - header.autohide


markdown_extensions:
    - meta
    - abbr

    - def_list                      # Les listes de définition.
    - attr_list                     # Un peu de CSS et des attributs HTML.
    - footnotes                     # Notes[^1] de bas de page.  [^1]: ma note.
    - admonition                    # Blocs colorés  !!! info "ma remarque"
    - pymdownx.details              #   qui peuvent se plier/déplier.
    - pymdownx.caret                # Passage ^^souligné^^ ou en ^exposant^.
    - pymdownx.mark                 # Passage ==surligné==.
    - pymdownx.tilde                # Passage ~~barré~~ ou en ~indice~.
    - pymdownx.highlight:           # Coloration syntaxique du code
        auto_title: true
        auto_title_map:
            "Python": "🐍 Script Python"
            "Python Console Session": "🐍 Console Python"
            "Text Only": "📋 Texte"
            "E-mail": "📥 Entrée"
            "Text Output": "📤 Sortie"
    - pymdownx.inlinehilite         # pour  `#!python  <python en ligne>`
    - pymdownx.snippets             # Inclusion de fichiers externe.
    - pymdownx.tasklist:            # Cases à cocher  - [ ]  et - [x]
        custom_checkbox:    false   #   avec cases d'origine
        clickable_checkbox: true    #   et cliquables.
    - pymdownx.tabbed:              # Volets glissants.  === "Mon volet"
        alternate_style: true 

    - pymdownx.keys:                # Touches du clavier.  ++ctrl+d++
        separator: "\uff0b"
    - pymdownx.emoji:               # Émojis  :boom:
        emoji_index:     !!python/name:materialx.emoji.twemoji
        emoji_generator: !!python/name:materialx.emoji.to_svg


    - pymdownx.superfences:
        custom_fences:
          - name: mermaid
            class: mermaid
            format: !!python/name:pymdownx.superfences.fence_code_format



    - pymdownx.arithmatex:
        generic: true
    - toc:
        permalink: ⚓︎
        toc_depth: 3

extra:
    social:
        - icon: fontawesome/brands/linux
          link: https://linuxfr.org/
          name: Promouvoir les Logiciels libres

        - icon: fontawesome/brands/wikipedia-w
          link: https://fr.wikipedia.org
          name: L'encyclopédie libre que chacun peut améliorer

        - icon: fontawesome/solid/paper-plane
          link: mailto:drane-site-lyon@region-academique-auvergne-rhone-alpes.fr
          name: Écrire à l'auteur



plugins:
  - search
  - awesome-pages:
        collapse_single_pages: true

extra_javascript:
  - xtra/mathjax.js                    # MathJax
  - https://cdn.jsdelivr.net/npm/mathjax@3/es5/tex-mml-chtml.js

extra_css:
  - xtra/ajustements.css                      # ajustements
