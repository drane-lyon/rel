# Textes de référence

La notion de ressources éducatives libres se fonde sur des textes de référence établis principalement par l'UNESCO. Nous vous proposons ici quelques sources pour appronfondir le sujet : 


## Textes de référence 

??? success "par l'UNESCO"

    - [Les ressources éducatives libres](https://www.unesco.org/fr/open-educational-resources) 
    - [Lignes directrices pour une politique des REL](https://unesdoc.unesco.org/ark:/48223/pf0000373887) 
    - [Ressources éducatives libres accessibles](https://unesdoc.unesco.org/ark:/48223/pf0000380471_fre) 
    - [Recommandations pour les ressources éducatives libres](https://www.unesco.org/fr/open-educational-resources) 
    - [Recommandations pour une science ouverte](https://unesdoc.unesco.org/ark:/48223/pf0000378841)

    
??? success "par l'OCDE et l'Union Européenne"

    - [Open Educational Resources Policy in Europa](https://oerpolicy.eu/) 
    - [Les ressources éducatives en libre accès, Pour diffuser gratuitement les connaissances](https://www.oecd.org/education/ceri/givingknowledgeforfreetheemergenceofopeneducationalresources.htm), CERI (texte intégral en anglais) 
    <br>Résumé en français : <https://www.oecd.org/fr/education/ceri/38851885.pdf>



## Ressources universitaires et sites incontournables


??? success "Du côté de la recherche"

    - [Chaire UNESCO RELIA](https://chaireunescorelia.univ-nantes.fr/) 
    - [Education ouverte et licences libres : protéger et partager](https://edunumrech.hypotheses.org/1481) 
    - [Brève histoire des REL](https://www.cairn.info/revue-administration-et-education-2015-2-page-119.htm) 
    - Wiley, David, [An Open Education Reader](https://openedreader.org/openedreader) 


??? success "Sites de référence"

    - [OERcommons](https://www.oercommons.org/) 
    - [La fabrique des REL](https://fabriquerel.org/rel/)  
    - [Universités Numériques Thématiques](https://unit.eu/nos-ressources/ressources-educatives-libres)



## Partagez vos références et vos ressources !

Partager ses sources d'informations, cela fait aussi partie des REL ! 
Vous trouverez dans [cette librarie partagée sur Zotero](https://www.zotero.org/groups/5006932/ressourceseducativeslibres/library) quelques références que chacun peut compléter.

**Rejoignez le groupe** pour pouvoir publier ! 
![Zotero-groupeREL.png](../Ressources/Zotero-groupeREL.png)






