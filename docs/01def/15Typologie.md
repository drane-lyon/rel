# Typologie des REL

Les REL peuvent être classées selon trois grandes catégories selon l'UNESCO :

* **Ressources d'apprentissage** : logiciel de cours, modules de contenus, objets d'étude, soutien aux étudiants et outils d'évaluation, et comme communautés d'étude en ligne ;
* **Ressources de soutien pour les enseignants** : outils pour les enseignants et matériels de support pour leur permettre de créer, d'adapter, et d'utiliser les REL, ainsi que comme matériaux de formation et autres outils d'enseignement pour enseignants ;
* **Ressources pour assurer la qualité** de l'éducation et des pratiques éducatives.


Selon l'OCDE : 
>Les ressources éducatives en libre accès comprennent le contenu pédagogique, les logiciels permettant de créer, d’utiliser et de diffuser le contenu, ainsi que les ressources de mise en œuvre telles que les licences automatiques. Le rapport donne à penser que les « ressources éducatives en libre accès » se rapportent à une somme d’actifs numériques qui peuvent être adaptés et génèrent des profits sans restreindre pour d’autres la possibilité d’en bénéficier.


## Tentative de typologie : 

A noter : La liste d'outils et de ressources proposée en dernière colonne n'est pas exhaustive, elle n'a pour but que de donner quelques exemples. 
![Typologie-REL.png](../Ressources/Typologie-REL.png)

***A noter** : le fichier sera bientôt disponible en format svg afin d'en permettre la modification.*