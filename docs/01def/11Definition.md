# Définition


## Qu'est-ce qu'une ressource numérique éducative ?

Une ressource numérique éducative est …

>… conçue pour les enseignants et les élèves à des fins d’enseignement ou d'apprentissage. Elle comprend ainsi des contenus, des services et/ou des outils associés. Elle doit répondre aux prescriptions des programmes et des référentiels de l'éducation nationale.
>Les droits d'utilisation et de réutilisation qui y sont attachés permettent un usage  en classe et/ou hors la classe. (_Source :_ Eduscol)

On peut retrouver les terminologies suivantes : Ressources Educatives Numériques, Ressources Numériques pour l'Ecole, Ressources d'enseignement et d'apprentissage, Ressources Numériques Pédagogiques.

Parmi ces ressources numériques, on peut distinguer les Ressources Éducatives Libres.


## Qu'est-ce qu'une Ressource éducative libre ?

### Définition de l'UNESCO, 2012

Énoncé pour la première fois au forum de l'UNESCO sur l'impact des didacticiels libres pour l'enseignement supérieur (Paris, juin 2002), les « REL » sont définies dans la *Déclaration de Paris* au Congrès mondial des ressources éducatives libres de 2012 : 

>Le terme REL désigne des matériels d’enseignement, d’apprentissage et de recherche sur tout support, numérique ou autre, existant dans le domaine public, ou publié sous une licence ouverte permettant l’accès, l’utilisation, l’adaptation et la redistribution gratuits par d’autres, sans restrictions ou avec des restrictions limitées.



### Critères de définition des REL


David Wiley propose 5 critères indissociables pour définir une Ressource Éducative Libre. Voici deux infographies qui permettent de visualiser ces critères :

![REL_douheret.png](../Ressources/REL_douheret.png)

[![REL-5R-fabriqueREL.png](../Ressources/REL-5R-fabriqueREL.png)](https://fabriquerel.org/wp-content/uploads/2020/06/REL-5R-fabriqueREL.pdf)



## Qu'avez-vous retenu ? 
Merci à la fabrique des REL d'avoir réalisé et de partager ce quiz :
<iframe src="https://fabriquerel.org/wp-admin/admin-ajax.php?action=h5p_embed&id=2" width="1170" height="364" frameborder="0" allowfullscreen="allowfullscreen" title="Jeu-questionnaire : Les REL"></iframe><script src="https://fabriquerel.org/wp-content/plugins/h5p/h5p-php-library/js/h5p-resizer.js" charset="UTF-8"></script>

