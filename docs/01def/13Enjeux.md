# Enjeux

Les enjeux présentés ici sont issus des recommandations sur les ressources éducatives libres de l'UNESCO, adoptées lors de sa conférence générale de Paris en 2012, se fondant sur les diverses déclarations de l'ONU et de l'UNESCO précédentes.


## Philosophie des REL
Les principes fondateurs des REL sont fondés sur la ***Déclaration universelle des droits de l’homme***, qui stipule : 

* le droit de chercher, de recevoir et de répandre (...) les informations et les idées (article 19)
* le droit à l’éducation (articles 26)
* le droit de prendre part librement à la vie culturelle de la communauté, de jouir des arts et de participer au progrès scientifique (article 27)
 
Les REL sont également issues des principes de la culture du libre et des préceptes de la notion de communs numériques. Ainsi participent-elles à promouvoir des enjeux :

* sociétaux, en se basant sur la coopération, l'intelligence et le contrôle collectifs ;
* économiques, en favorisant l'innovation et en n'étant pas soumis aux logiques "propriétaires" ;
* stratégiques, par la conservation de la souveraineté et du contrôle des données.


## Enjeux pour l'éducation


??? success "Pour une éducation ouverte" 

	L'UNESCO rappelle le besoin de "faciliter la libre circulation des idées, par le mot et par l’image". 
    Les REL participent ainsi aux "**pratiques éducatives ouvertes**", basées sur les pédagogiques ouvertes, qui promeuvent les modèles pédagogiques innovants, respectant et responsabilisant les apprenants, les incitant à coopérer. 
    L'UNESCO, dans sa déclaration du Cap, précise que « l'éducation ouverte ne se limite pas aux seules ressources éducatives ouvertes. Il s'appuie également sur des technologies ouvertes qui facilitent l'apprentissage collaboratif et flexible et le partage ouvert des pratiques d'enseignement qui permettent aux éducateurs de bénéficier des meilleures idées de leurs collègues. Il peut également se développer pour inclure de nouvelles approches d'évaluation, d'accréditation et d'apprentissage collaboratif ».


??? success "Pour une meilleure inclusion" 

	L'UNESCO rappelle que "les ressources éducatives libres (REL) peuvent favoriser une éducation de qualité équitable, inclusive, ouverte et participative". Pour cela, elles "doivent être non discriminatoires, propices à l’apprentissage, conçus pour l’apprenant, adaptés en fonction du contexte, économiques et accessibles à tous les apprenants – enfants, jeunes et adultes". 
    Ainsi, les 5R permettent-elles aux enseignants d'adapter les REL en fonction de son contexte d'enseignement et de son public pour en favoriser l'accessibilité, la mise à jour, en les remixant et en les redistribuant.



??? success "Au service des enseignants" 

	L'UNESCO insiste également sur le renforcement des libertés académiques et de l’autonomie professionnelle des enseignants "en élargissant la gamme des supports d’enseignement et d’apprentissage disponibles" et "devraient jouer le rôle essentiel dans le choix et la mise au point du matériel d’enseignement, le choix des manuels et l’application des méthodes pédagogiques". 
    La création, la modification, le partage de ressources par les enseignants permet donc à chacun d'exercer sa liberté pédagogique.



??? success "Vers une société du savoir" 

	Enfin, l'UNESCO rappelle le programme de développement durable à l'horizon 2030 de l'ONU inscrit que "l’expansion de l’informatique et des communications et l’interdépendance mondiale des activités ont le potentiel d’accélérer les progrès de l’humanité, de réduire la fracture numérique et de donner naissance à des sociétés du savoir." 
    Les REL participent ainsi à la diffusion des savoirs par leur accessibilité, par leurs **formats ouverts** et par les droits de modification (de traduction par exemple) et de redistribution.


## Enjeux sociétaux 


### L'objectif de Développement Durable (ODD)  

L'UNESCO rappelle que les REL participent à « la réalisation de l’objectif de développement durable 4 (ODD 4), qui appelle la communauté internationale à assurer à tous une éducation équitable, inclusive et de qualité et des possibilités d’apprentissage tout au long de la vie ». 

Les REL permettent également de répondre à différents objectifs du Programme de développement durable à l'horizon 2023 de l'ONU. 


??? success "Les découvrir" 

	![F-WEB-Goal-04.png](../Ressources/F-WEB-Goal-04.png)
    ![F-WEB-Goal-05.png](../Ressources/F-WEB-Goal-05.png)
    ![F-WEB-Goal-07.png](../Ressources/F-WEB-Goal-07.png)
    ![F-WEB-Goal-08.png](../Ressources/F-WEB-Goal-08.png)
    ![F-WEB-Goal-10.png](../Ressources/F-WEB-Goal-10.png)
    ![F-WEB-Goal-16.png](../Ressources/F-WEB-Goal-16.png)


### Vers un numérique responsable

Les REL sont des vecteurs d'action pour un numérique plus responsable par : 

- la sobriété : en partageant une ressource en format ouvert, cela évite de créer ou de reproduire des ressources de même nature. 
- l'accessibilité : les principes des REL agissent en faveur de l'inclusion. 
- l'éthique que les REL véhiculent


### Pour une information libre

Les enjeux de citoyenneté et d'accès à une information plurielle sont forts et les REL participent au développement de  : 

- La liberté d'expression 
- La liberté de la presse 
- La liberté de conscience

### Enjeux économiques

Les REL participent également à la construction de modèles économiques basés sur : 

- société du partage 
- gratuité, modification et redistribution
