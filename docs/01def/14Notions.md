# Notions



## Open-source
La dénomination *open source* s'applique plus particulièrement aux logiciels. Pour être défini comme *open source*, un logiciel doit avoir une licence qui correspond à la définition de l'[Open Source Initiative](https://opensource.org/)

L'expression *open source* a été introduite en 1998 par Christine Peterson pour lever les ambiguités de l'expression *free software* qui désigne à la fois le logiciel libre tel que défini précédemment et la gratuité d'un logiciel.


----
## Les communs numériques


Selon la [Mission Société Numérique](https://communs.societenumerique.gouv.fr/#section-h2-03) : "Un commun désigne une ressource produite et/ou entretenue collectivement par une communauté d’acteurs hétérogènes, et gouvernée par des règles qui lui assurent son caractère collectif et partagé. 
Il est dit numérique lorsque la ressource est numérique : logiciel, base de données, contenu numérique (texte, image, vidéo et/ou son), etc."

??? success "Des précisions"

    Le collectif PointsCommuns propose la définition suivante de "*communs*" sur son [portail](https://lescommuns.org/) :
    >Les **biens communs**, ou tout simplement communs, sont des ressources, gérées collectivement par une communauté, celle-ci établit des règles et une gouvernance dans le but de préserver et pérenniser cette ressource. Il précise que cette définition évolue et recoupe divers angles et s'adapte à de nombreux domaines de la connaissances. Différentes définitions sont proposées sur [cette page](http://notesondesign.org/biens-communs-definitions/)

	Sur la question des communs numériques dans l'éducation plus précisément, nous pouvons retenir les définitions de : 

	- **Wikipedia** :  Les biens communs numériques correspondent à l'ensemble des ressources numériques produites et gérées par une communauté.
	- **Louise Merzeau** : La notion de biens communs informationnels pose la question des ressources et des modalités de partage de ces ressources. Notamment parce que les ressources numériques posent un problème particulier, dans la mesure où celles-ci sont constitutivement partageables, de par leur reproductibilité et leur viralité.

	La notion de biens communs induit également la notion de communauté, cette dernière manifestant une certaine intention et volonté de partager ces ressources.

	Il s’agit donc d’interroger le lien entre ressources et communauté, en tant qu’elles servent pour une communauté à définir un cadre d’échange, de partage et finalement, à terme, à se définir elle-même.

	![Communs.png](../Ressources/Communs.png)



----

## Numérique responsable

Les REL sont des composantes du Numérique responsable par leurs objectifs et leur définition.
Ainsi, dans ses [recommandations pour les REL](https://www.unesco.org/fr/legal-affairs/recommendation-open-educational-resources-oer), l'UNESCO rappelle que les REL répondent aux objectifs du numérique responsable dont l'objectif 4 de Développement Durable de l'ONU.



??? success "Les Objectifs de Développement Durable (ODD)"

    ![F-WEB-Goal-04.png](../Ressources/F-WEB-Goal-04.png){ align=left } 
    
        L'objectif 4 "**Une éducation de qualité**" rappelle les points suivants : 
    	- 4.1. Suivi d'un cycle d'enseignement primaire et secondaire gratuit et de qualité. 
        - 4.2. Accès à des activités de développement et de soins de la petite enfance. 
        - 4.3. Accès à un enseignement technique, professionnel ou tertiaire, y compris universitaire, de qualité et d'un coût abordable.
        - 4.4. Augmentation du nombre d'adultes disposant de compétences (..) nécessaires à l'emploi, à l'obtention d'un travail décent et à l'entrepreneuriat.
        - 4.5. Élimination des inégalités de sexe et accès à l'éducation des personnes vulnérables, à tous les niveaux d'enseignement.
        - 4.6. Acquisition pour tous des compétences : lire, écrire, compter.
        - 4.7. Acquisition par les élèves des compétences nécessaires pour promouvoir le développement durable, dans toutes ses composantes.
    
	![E_SDG_poster_UN_emblem_WEB.png](../Ressources/E_SDG_poster_UN_emblem_WEB.png)




---


## Une définition du libre ?

### Définition(s)

??? success "Le terme “libre” selon le CNTRL"

	Selon le CNTRL, l'adjectif "libre" a différentes déclinaisons, dont on peut citer quelques exemples : 

	- **lié à l'individu** : "*Qui n'est pas soumis à une ou plusieurs contraintes externes*" ou plus précisément, s'agissant d'un individu membre d'une société politique, "*Qui n'est pas soumis à la puissance contraignante d'autrui*".
	- **lié à la personne** : "*Qui est disponible*, *maître de*, *qui a la faculté, la possibilité, le droit de...*
	- **lié à un processus, une action** : *Qui s'accomplit sans contrainte ou dont l'évolution (le fonctionnement, la manifestation, le mouvement) n'est pas gêné, empêché.*" ou encore "*Qui ne présente pas d'obstacle(s) limitant l'accès, le passage, la circulation.*"

Quelques points à retenir: 
![Libre.png](../Ressources/Libre.png)

??? success "Points de vigilance"
	- À noter : D'après le ministère de l'économie et des finances, la mention « libre de droits » ne signifie pas que l’utilisation d’un contenu n'est pas conditionnée.
	- Il s'agit d'un type d'offre commerciale qui permet à un utilisateur (sous réserve d'un abonnement par exemple) d'utiliser des contenus selon un cadre fixé par l'auteur ou le distributeur.
	- Les contenus « libres de droits » sont donc différents des contenus sous licences libres (ex: _creatives commons_).


### Logiciel libre

L'expression "Logiciel libre" a été initiée par [Richard Stallmann](https://fr.wikipedia.org/wiki/Richard_Stallman) et fait référence à la faculté d'un logiciel de permettre à un usager d'en garder le contrôle. 

??? success "Le logiciel libre est basé sur une licence qui doit répondre à quatre critères :" 

	- La liberté d’exécuter le logiciel pour tous les usages ;
	- La liberté d’étudier et de modifier son fonctionnement, notamment pour vérifier son fonctionnement et pour l'adapter à ses besoins ;
	- La liberté de redistribuer des copies exactes ;
	- La liberté de distribuer des versions modifiées, pour en faire profiter toute la communauté.


### Culture libre

La culture libre est un courant de pensée qui promeut et agit en faveur de l'égalité des hommes pour accéder, distribuer, modifier des "*oeuvres de l'esprit*" et défend l'idée que les droits d'auteurs ne doivent pas porter atteinte aux libertés fondamentales du public. 

??? success "Des précisions"
	Ce mouvement se base sur la philosophie du logiciel libre et l'étend à tout média, à toute la culture, à toute information dans les domaines encyclopédique, artistique, éducatif, scientifique, économique, etc..
	Cette culture est portée par les *libristes* parmi lesquels on peut citer par exemple Aaron Schwartz, Tristan Nitot, Alexis Kauffmann.


#### L'ExpoLibre par l'April

Cette exposition en libre accès donne les contours essentiels pour appréhender la culture du libre : 

??? success "L'ExpoLibre"

	![](https://minio.apps.education.fr/codimd-prod/uploads/upload_7c6e9a3d8a04424768bc2cd1af712ebc.jpg)

	![](https://minio.apps.education.fr/codimd-prod/uploads/upload_a0c9ddacd328fe923e118abb5b932f7a.jpg)

	![](https://minio.apps.education.fr/codimd-prod/uploads/upload_2eccf7e53cda4e5a0231e11994016bab.jpg)

	![](https://minio.apps.education.fr/codimd-prod/uploads/upload_127f0a62b53cbab4480a9bd1577754ab.jpg)

	![](https://minio.apps.education.fr/codimd-prod/uploads/upload_2fb56030303d832c1189de1146c769e0.jpg)

	![](https://minio.apps.education.fr/codimd-prod/uploads/upload_7613e9828128ca9a3cf184d5d17ce3f6.jpg)

	![](https://minio.apps.education.fr/codimd-prod/uploads/upload_1b97b183f2ed70dce595c9bc894fc14b.jpg)

	![](https://minio.apps.education.fr/codimd-prod/uploads/upload_4487b86a978eaf32b59a20f6644efcba.jpg)












