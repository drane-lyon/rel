---
author: et Mireille Coilhac
title: Squelette vide
---

## Titre 1 : Attention

Avec la version actuelle, il ne faut pas mettre une admonition IDE dans une autre admonition.

### Titre 2

Un tableau

<table>
    <tr>
        <td style="border:1px solid; font-weight:bold;">Poire</td>
        <td style="border:1px solid; font-weight:bold;">Pomme</td>
        <td style="border:1px solid; font-weight:bold;">Orange</td>
        <td style="border:1px solid; font-weight:bold;">Kiwi</td>
        <td style="border:1px solid; font-weight:bold;">...</td>
    </tr>
</table>



Avec #!py : Le type `#!py list` de Python   
Sans : Le type `list` de Python   

!!! warning "Remarque"

    blabla indenté



```pycon
>>> nombres = [3, 8, 7]
>>> fruits = ['Poire', 'Pomme', 'Orange', 'Kiwi']
```



!!! danger "Attention"

    blabla indenté

blablabla


??? note "se déroule en cliquant dessus"

    blabla indenté


??? tip "Astuce"

    Ma belle astuce 


## QCM

???+ question

    On considère le tableau `meubles = ['Table', 'Commode', 'Armoire', 'Placard', 'Buffet']`.

    === "Cocher la ou les affirmations correctes"
        
        - [ ] `#!py meubles[1]` vaut `#!py 'Table'`
        - [ ] `#!py meubles[1]` vaut `#!py 'Commode'`
        - [ ] `#!py meubles[4]` vaut `#!py 'Buffet'`
        - [ ] `#!py meubles[5]` vaut `#!py 'Buffet'`

    === "Solution"
        
        - :x: Les indices débutent à `#!py 0`. Donc `#!py 'Table'` est à l'indice `#!py 0`
        - :white_check_mark: `#!py meubles[1]` vaut bien `#!py 'Commode'`
        - :white_check_mark: `#!py meubles[4]` vaut bien `#!py 'Buffet'`
        - :x: Le tableau contient 5 éléments. Le dernier éléments est donc à l'indice `#!py 4`


 
        
## Notebooks : sujet à télécharger et corrections cachées

???+ question "Les notes d'Alice - Saison 2"

    Les notes d'Alice : saison 2
    Mettre les fichiers dans un dossier a_telecharger. Bien indiquer le bon chemin

    🌐 TD à télécharger : Fichier `mon_fichier_sujet.ipynb` :
    
<!-- ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_sujet.ipynb) -->


La correction est cachée dans les commentaires

<!--- La correction à télécharger plus tard

        🌐 Fichier `mon_fichier_corr.ipynb` : ["Clic droit", puis "Enregistrer la cible du lien sous"](a_telecharger/mon_fichier_corr.ipynb)

        -->

