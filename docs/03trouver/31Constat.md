# Constat


Trouver des REL n'est pas forcément aisé. Il existe de multiples espaces de partage, de sites webs, de comptes personnels sur les réseaux sociaux à des formes plus ou moins organisées. Par ailleurs, il existe des REL qui ne sont pas indexées en tant que telles ou au contraire des productions étiquetées comme REL mais qui n'en sont pas réellement.

Comment s'y retrouver ?

![univers-hubble_BY-SA.jpg](../Ressources/univers-hubble_BY-SA.jpg)

Source :"Hubble’s colourful view of the Universe" by Hubble Space Telescope / ESA is licensed under CC BY 2.0. 

