# Initiatives institutionnelles

La mutualisation et le partage de ressources éducatives s'exerce à la fois sur des espaces de partage informels (sites personnels d'enseignants, réseaux sociaux...) mais également organisés par l'institution.

Nous pouvons citer par exemple :



??? success "Edubase"

    "Édubase est une banque de scénarios pédagogiques académiques conçus par des enseignants pour des enseignants." Source : [Eduscol](https://edubase.eduscol.education.fr/credits)

    Edubase a été créée en 2003 à l'initiative de la Direction du Numérique pour l'Éducation. Elle est alimentée par les Interlocuteurs Académiques au Numérique et propose de nombreux scenarios pédagogiques sous licence Creative Commons.

    Les objectifs de sa création sont triples : 
    - contribuer au développement des compétences professionnelles et à la formation des enseignants pour le développement de démarches pédagogiques mobilisant le numérique 
    - mettre à disposition des séances de qualité, validées par l’inspection, dans le cadre des programmes.
    - enrichir le corpus des exemples en s'appuyant sur la mutualisation inter-académique.


??? success "M@gistère"

    La plateforme de formation en ligne M@gistère, basée sur le logiciel libre Moodle, permet aux enseignants de se former pour développer leurs pratiques professionnelles, dans différents domaines.



??? success "La Forge des Communs Numériques Éducatifs"

    **« Que la forge soit avec nous… »**
    Envisagée comme un commun numérique, la Forge des Communs Numériques Éducatifs (FCNE) a pour objectif de mettre à disposition des enseignants une forge GitLab en gouvernance partagée entre le Ministère et la communauté de ses utilisateurs. 
    La forge fait partie des objectifs de la Stratégie du numérique pour l'éducation 2023-2027 du ministère de l'Éducation nationale. [En savoir plus](https://docs.forge.apps.education.fr)
    
    **C'est en forgeant que l'on devient forgeron !**




??? success "ELEA"

    ELEA est un espace de conception de cours à distance basé sur le logiciel Moodle et adapté aux établissements scolaires. Développé comme un commun numérique, il sera prochainement disponible pour tous les établissements du second degré sur le territoire français.





