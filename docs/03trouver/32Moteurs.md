# Moteurs de recherche dédiés

Vous trouverez ci-dessous quelques banques de REL.

???+ Warning 

   A noter : la plupart des banques de REL présentées ici sont de niveau universitaire et à échelle mondiale. 
   Le recensement de REL pour les premier et second degrés reste à développer. Participez en vous inscrivant [ici](https://framaforms.org/rel-contribuer-1680415614) !




## Les espaces de partage de REL

??? success "A l'échelle internationale"

    - [Elibrary de l'UNESCO](https://elibrary.iite.unesco.org/#/) 
    - [OER Platform](http://www.oerplatform.org/)
    - Les [REL du MIT](https://ocw.mit.edu/)
    - [OER in AFrica](https://www.oerafrica.org/oer-initiatives-africa), Méta-moteur
    - [ORELT](https://www.colorelt.org/), ressources en langue anglaise pour enseigner l'anglais
    - [Share my lesson](https://sharemylesson.com/), en anglais
    - [Open Knowledge Repository Beta](https://openknowledge.worldbank.org/home)
    - Les [REL du TESSA](https://www.tessafrica.net/home), disponibles en plusieurs langues



??? success "Pour les francophones"

    - Le [catalogue de ressources UNIT](ressources.unit.eu/moteur-ressources-educatives-libres/advanced-search.html?submenuKey=advanced&search=true&menuKey=lom&userChoices%5Bsimple_all%5D.simpleValueRequestType=default&userChoices%5Bsimple_all%5D.simpleValue=%q%) 
    - [Abuledu DATA](http://data.abuledu.org/wp/ressources-educatives-libres/)
    - Le [catalogue de Education et Numérique](https://edunum.apolearn.com/catalogue/)
    - le [catalogue du CERES](http://ceres.vteducation.org/app/?lang=fr)






## Ressources libres utilisables en milieu scolaire

D'autres structures proposent des ressources réutilisables en milieu scolaire. Par exemple* : 

- La BnF, sur son [site Gallica](https://gallica.bnf.fr/accueil/fr/content/accueil-fr?mode=desktop)
- [Europeana](https://www.europeana.eu/fr), Décourvrir le patrimoine culture européen
- [La Main à la pâte](https://fondation-lamap.org/)
- [LearningApps](https://learningapps.org/)


* Liste non exhaustive [à compléter](https://framaforms.org/rel-contribuer-1680415614) 







