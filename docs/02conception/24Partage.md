# Partage des REL

Le partage des REL peut se penser en fonction du public visé et de la communauté impliquée. Dans tous les cas, il est judicieux d'y apposer une licence.


## Les licences

### Les licences Creative cCommons

![cc-logo.jpg](../Ressources/cc-logo.jpg){ align=left }

Pour respecter les critères des REL, il est préférable de choisir des licences les plus ouvertes possibles 

??? success "Choisir sa licence"

    Cédric Frayssinet a traduit cette infographie permettant de choisir sa licence CC en fonction des droits que l'on souhaite accorder.
    ![REL_Choose_Licence_CC_fr.png](../Ressources/REL_Choose_Licence_CC_fr.png)

    Perrine Douhéret propose celle-ci, plus adaptée aux REL : 
    ![Licence-CC-Choix-perrine.jpeg](../Ressources/Licence-CC-Choix-perrine.jpeg)


??? success "Comprendre les licences creative commons en vidéo"

    Le Ministère de la Culture propose cette vidéo pour comprendre les licences CC : 

    <div style="position:relative;padding-bottom:56.25%;height:0;overflow:hidden;"> <iframe style="width:100%;height:100%;position:absolute;left:0px;top:0px;overflow:hidden" frameborder="0" type="text/html" src="https://www.dailymotion.com/embed/video/x1tg4gv" width="100%" height="100%" allowfullscreen title="Dailymotion Video Player"> </iframe> </div>




### La licence Etalab

![licence-ouverte-open-licence.gif](../Ressources/licence-ouverte-open-licence.gif){ align=left }

!!! info "Etalab" 

    Etalab est un département de la DINUM, qui "coordonne la conception et la mise en œuvre de la stratégie de l’État dans le domaine de la donnée". Une licence spécifique pour l'utilisation et le partage des données ouvertes de l'Etat a été élaborée en concertation avec les différents acteurs : La [Licence Ouverte](https://www.etalab.gouv.fr/licence-ouverte-open-licence/).





## Communauté et espace de partage

Dans l'idéal, une REL doit pouvoir être partagée, modifiée, abondée, réutilisée à une échelle la plus large possible.

??? success "Dans un établissement"

    Si la REL ne concerne que la communauté scolaire de son établissement, il est possible d'utiliser les outils de son écosystème numérique comme l'ENT ou Moodle pour les partager.


??? success  "Dans un réseau d'établissement, sur un territoire particulier"

    Les ENT peuvent proposer des espaces inter-établissements pour permettre le partage et la co-conception de REL entre professeurs et élèves de différents établissements utilsant la même solution ENT.


??? success  "Au sein d'une discipline, à l'échelle académique"
    Les sites académiques disciplinaires ou thématiques peuvent être des espaces de partage des REL. 


??? success  "Pour une communauté nationale"
    À l'échelle nationale, plusieurs espaces de partage et de conception de REL sont en train de se construire : 
    
    - Les Edubases proposent depuis longtemps des scenarios pédagogiques partagés, à adapter en fonction du contexte de son établissement
    - Le nouveau bien commun ELEA, basé sur le service Moodle, devrait proposer un espace de partage de cours. Un réseau des concepteurs ELEA pourrait initier ce partage de REL.
    - La [Forge des communs numériques éducatifs](https://forge.apps.education.fr/), créée en 2024, permet à des enseignants de créer des REL et de les partager au sein d'une forge basée sur GitLab. 


??? success  "Pour une communauté plus large"
    - Les forges GitLab et GitHub permettent le partage, l'appropriation et la modification de REL.
    - Les réseaux sociaux sont également des vecteurs de partage de REL : de nombreux enseignants y partagent leurs ressources pédagogiques.
    - Les sites web personnels d'enseignants permettent également de partager des REL.







## Indexer les REL pour les valoriser

Pour favoriser le partage des REL, il est nécessaire de les indexer le plus précisément possible et de respecter les normes internationales. Ces métadonnées permettent de retrouver efficacement les ressources numériques. Plusieurs normes existent et ont été déclinées pour un usage éducatif.

[Les Universités Numériques Thématiques](https://unit.eu/) en expliquent les enjeux, les normes et les outils sur [cette page](https://unit.eu/nos-ressources/valoriser-des-ressources).


??? success  "Dublin Core"
    Créé en 1995, cette norme vise à permettre une interopérabilité entre les différents systèmes. Il s'agit de renseigner des éléments permettant d'identifier une ressource par 15 items, portant à la fois sur la description du contenu, de la propriété intellectuelle et de l'instanciation.
    Pour en savoir plus, vous pouvez consulter cette [page du site de la BnF](https://www.bnf.fr/fr/dublin-core#bnf-objectif-du-dublin-core) ou ce [document publié par l'ENSSIB ](https://www.enssib.fr/bibliotheque-numerique/documents/1236-presentation-des-standards-le-dublin-core-dc.pdf)


??? success  "ScoLOM"
    Le Learning Object Metadata (LOM) a été publié en 2002 par le Learning Technology Standards Commitee (LTSC) de l’IEEE (Institute of Electrical and Electronics Engineers) et a pour but de décrire les ressources numériques. 
    Le [ScoLOMFR](https://www.reseau-canope.fr/scolomfr/), issu du LOM, a pour but d'indexer les ressources pédagogiques numériques afin d'en permettre une découverte et une recherche efficace et adaptée aux besoins des enseignants.
    Pour en savoir plus, vous pouvez consulter ce [document publié par l'ENSSIB](https://www.enssib.fr/bibliotheque-numerique/documents/1237-presentation-des-standards-lom-learning-object-metadata.pdf) sur le LOM et le site [ScoLOMFR](https://www.reseau-canope.fr/scolomfr/quest-ce-que-scolomfr) créé par le Réseau Canopé.




