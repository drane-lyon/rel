# Outils pour construire des REL

## Quels critères pour choisir son outil de conception ?

Un principe : Rendre la REL **accessible** !

- S'assurer des modalités d'accès à l'outil : licence, coût, version, ...
- S'assurer des modalités d'utilisation du point de vue technique: téléchargement, installation, connexion Internet...
- S'assurer des modalités d'usage : facilité de prise en main, responsivité, adaptations possibles...

De manière générale, privilégier des outils open-source.


## Où trouver des outils ?

Liste non exhaustive ;-)

??? success "Apps.education.fr"

    Le portail [Apps.education.fr](https://portail.apps.education.fr/) propose de nombreux outils open-source adaptés aux besoins des enseignants et personnels :
    ![REL_Apps.png](../Ressources/REL_Apps.png)

    Avec CodiMD, il est possible de créer de nombreux documents à éditer collaborativement. Cédric Eyssette vous propose [différents modèles](https://codimd.apps.education.fr/5vGt6F4zRjOyf7wAwfC1og).
    Vous pouvez vous connecter à ces outils avec vos identifiants académiques.


??? success "La Digitale"

    Merci Emmanuel Zimmert qui développe des [outils simples et utiles](https://ladigitale.dev/) pour les enseignants :
    ![REL_Digitale.png](../Ressources/REL_Digitale.png)


??? success "SILL"

    Le Socle Interministériel de Logiciels Libres propose également 335 logiciels dans son [catalogue](https://sill.etalab.gouv.fr/software) :
    ![REL_SILL.png](../Ressources/REL_SILL.png)


??? success "Collectif Chatons"

    Le [Collectif Chatons](https://www.chatons.org/presentation) propose  de nombreux [services alternatifs](https://www.chatons.org/search/by-service) pour s'affranchir des GAFAM. 



## Un outil, des formats

Plus que l'outil pour construire sa REL, il est nécessaire de penser au format de partage ! 

- [OpenDocument](https://fr.wikipedia.org/wiki/OpenDocument) est le format le plus ouvert : il permet d'éditer les documents. C'est le format adopté par les suites bureautiques libres.
- PDF : ce format permet la lecture de documents à partir de tout type de matériel. 
- SVG : permet la modification facile d'images.







