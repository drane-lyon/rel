# Validation des REL


Une REL doit-elle être validée pour exister ? et selon quelles modalités ?

## Pourquoi valider les REL ?
Deux cas peuvent se présenter : 

- un partage d'initiative personnelle : charge  à l'auteur de s'assurer du respect des 5R. C'est la communauté qui pourra promouvoir, modifier, enrichir cette REL et la faire vivre.
- un partage dans un espace institutionnel : la validation est alors du ressort de l'institution qui héberge.


Dans tous les cas, l’étape de validation d’une ressource éducative libre est nécessaire avant de la mettre en partage. Il s’agit ici de s’assurer d’une part, que les règles des ressources éducatives libres sont respectées et d’autre part, que le contenu proposé est pertinent.

## Quels critères pour valider les REL ?

### une ressource libre
Ici, il convient de vérifier la présence des éléments suivants :

- Indication de la **paternité** 
- Apposition d’une **licence compatible avec une REL** et avec celles des ressources réemployées 
- Respect de la [**méthode TASL**](https://pedagogie.ac-nantes.fr/medias/fichier/poster-methode-tasl-compressed_1669727466447-pdf) en cas de réemploi de ressources sous licence libre 
- Respect du droit à l’image, du droit d’auteur, du RGPD 
- Mention des contributeurs éventuels
Il faut s’assurer que le format est ouvert et facilement modifiable.


??? success "En savoir plus la méthode TASL"

   
    ![TASL.png](../Ressources/TASL.png)


Exemple : 
Le partage de modèles pour créer tout type de créations en Markdown : <https://codimd.apps.education.fr/5vGt6F4zRjOyf7wAwfC1og>, par Cédric Eyssette



### Une ressource éducative de qualité

- Indication des sources et des références bibliographiques 
- Mise à l’épreuve de la ressource face au public visé et retour d’expérience 
- Relecture et évaluation par des pairs, et/ou par des experts (IA-IPR, DGESCO...), … 
- Réalisation des modifications demandées jusqu’à validation définitive.

Un exemple significatif : 
Manuels scolaires en Sciences et Techniques de Laboratoire (STL) sous Moodle, supervisé par la DGESCO : <https://spcl.ac-montpellier.fr/moodle/>


Les contributeurs pourraient endosser la validité de la ressource afin de lui donner une plus grande légitimité.


