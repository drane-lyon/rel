# Principes directeurs

Lors de la conception d'une Ressource Éducative Libre, il est nécessaire de s'appuyer sur les 5R, critères les définissant : 

![REL_douheret.png](../Ressources/REL_douheret.png)


## Les questions à se poser dès la conception : 

- Pour quelle communauté ? Cela va définir les outils et les espaces de partage de la REL.

- Quelles ressources intégrer et quelles licences y sont associées ? 

- Comment rendre la ressource accessible au plus grand nombre ?