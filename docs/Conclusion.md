# Conclusion du travail...


Cette Ressource Éducative Libre est destinée à être amendée, complétée, enrichie pour s'inscrire dans la philosophie des REL.

N'hésitez pas à contribuer ! 

Rejoignez-nous sur la forge ou contactez-nous via [ce formulaire](https://framaforms.org/rel-contribuer-1680415614) !
