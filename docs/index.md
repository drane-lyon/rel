# Bienvenue sur ce site web consacré aux Ressources Éducatives Libres


Logo des ressources éducatives libres par [Jonathas Mello](https://fr.wikipedia.org/wiki/Ressources_%C3%A9ducatives_libres#/media/Fichier:Logo_Ressources_Educatives_Libres_(REL)_mondial.svg) sous licence CC BY 3.0.

![](https://upload.wikimedia.org/wikipedia/commons/a/af/Logo_Ressources_Educatives_Libres_%28REL%29_mondial.svg)



## Pourquoi ce site ? 
De nombreuses ressources sont à disposition des enseignants pour enseigner avec et par le numérique. Les enseignants partagent également de nombreuses ressources via Edubase, des blogs personnels, sur les réseaux sociaux... 
Cependant, il n'est pas toujours simple de savoir ce que l'on peut utiliser, modifier ou partager et dans quel contexte.

Par ailleurs, la Direction du Numérique pour l'Éducation investit pleinement le champ du libre éducatif dans [sa stratégie nationale parue en janvier 2023](https://www.education.gouv.fr/strategie-du-numerique-pour-l-education-2023-2027-344263), en mettant à disposition des enseignants des outils de création de ressources éducatives libres (REL).

La [DRANE Site de Lyon](https://dane.ac-lyon.fr/spip/), déjà engagée dans l'acculturation aux communs numériques dans l'éducation, vous propose ce dossier compilant un maximum d'informations pour promouvoir les ressources éducatives libres.


## Que trouver ici ?

* des informations générales sur les REL 
* des principes et outils pour concevoir des REL
* des exemples de REL et des outils pour en retrouver


## Pour quel public ?
L'objectif est de permettre au plus grand nombre de comprendre, trouver ou concevoir des REL.

Ce site est donc destiné à tout enseignant, mais également aux cadres et formateurs souhaitant impulser la création et le partage de ressources entre pairs dans un cadre le plus adaptable et accessible possible. 


## Comment est-il conçu ?
Élaboré comme une REL, ce site est réalisé sur la Forge des Communs Numériques Éducatifs de l'Éducation nationale (<https://forge.apps.education.fr/>). La conception originelle est réalisée par la DRANE Site de Lyon, en collaboration avec la DNE et l'équipe de la forge. 

Ce site est destiné à évoluer avec la participation de volontaires que nous pourrons intégrer à la communauté de rédaction. 
**Vous souhaitez rejoindre notre communauté ?** Merci de renseigner [ce formulaire](https://framaforms.org/rel-contribuer-1680415614)



***Pour naviguer, utiliser le menu en haut de page.***

