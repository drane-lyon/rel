# Votre site mkdocs

## À faire à la première utilisation

Ce projet est un modèle pour publier un premier site avec [mkdocs](https://mkdocs.org).

- [ ] Modifier les métadonnées dans [`mkdocs.yml`](mkdocs.yml)
    - [ ] Changer nom du site
    - [ ] Changer le nom de l'auteur
- [ ] Rédiger une présentation du site sur [docs/index.md](./docs/index.md)
- [ ] Parcourir les chapitres
    ```
    docs/index.md
        /chapitre1/index.md
                /activite.md
                /cours.md
    ``` 
- [ ] Modifier [docs/chapitre01/.pages](.docs/chapitre01/.pages) pour donner un titre plus précis à votre chapitre 
- [ ] Consulter [docs/canevas/squelette.md](docs/canevas/squelette.md) l'aide mémoire des principales instructions de mise en forme de `mkdocs`.

## Méthode de publication du modèle

Ce modèle permet de publier un site web de façon collaborative en tirant intensivement parti des fonctionnalités de _<dfn style="text-decoration: underline" title="Requête de fusion : demande de fusionner une branche dans une autre">merge request</dfn>_ et de commenter les modifications apportées.

1. Message décrivant les modifications
2. Créer un nouvelle branche est sélectionner
3. On choisit le nom de la branche
4. On clique sur «commit»
    ![](./doc/creation_commit_initial_branch.png)
5. On vérifie que les branches sont les bonnes
6. Le message précédent a été répliqué ici
7. Scindé en deux parties
8. Je recommande de faire «assign to me», ça permet de le retrouver aisément
9. S'il y a un relecteur particulier qu'on souhaite solliciter
10. Cocher la case «delete …» pour éviter d'avoir des branches mortes en pagaille
11. Créer la demande de fusion
    ![](./doc/creation_requete_fusion.png)
12. On peut aller voir les modifications initiales
13. De préférence en mode «côte à côte»
    ![](./doc/visualisation_modifications_initiales.png)
14. Le site se construit
    ![](./doc/construction_site.png)
15. Et dessous, on pourra voir le bouton apparaître
    ![](./doc/visualisation_site.png)
16. À tout moment, on peut consulter les modifications
    ![](./doc/selectionner_texte_a_commenter.png)
    Et sélectionner une portion de texte pour la commenter
17. Et envoyer tous les commentaires d'un coup, d'un seul
    ![](./doc/repondre_a_un_commentaire.png)
18. Dans le cas d'une «suggestion», la modification proposée apparaît dans son contexte
    ![](./doc/voir_suggestion.png)

Pour produire du contenu, on peut consulter [le tutoriel de Franck
Chambon](https://ens-fr.gitlab.io/tuto-markdown/premier/) ou le [tutoriel
avancé](https://ens-fr.gitlab.io/mkdocs/) du même auteur.
